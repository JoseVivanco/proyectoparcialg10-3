/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaz;

import java.util.ArrayList;

/**
 *
 * @author Brank
 */
public class MenuEmpresas extends Menu{

    public MenuEmpresas(ArrayList<String> opciones) {
        super(opciones);
    }

    public static ArrayList<String> añadirOpciones(){
        ArrayList<String> opciones = new ArrayList<>();
        opciones.add("Todos");
        opciones.add("Por nombre");
        opciones.add("Por categoría");
        opciones.add("Por ciudad y sector");
        opciones.add("Regresar al menú principal");
        return opciones;
    }    
}
