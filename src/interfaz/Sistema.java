/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaz;

import entidades.*;
import java.util.*;
import usuarios.*;

/**
 *
 * @author Brank
 */
public class Sistema {

    public static Persona usuarioActivo = null;

    public static void cargarSistema() {
        inicializarSistema();
        boolean salir = false;
        Integer opcion;
        MenuAutenticacion menuAutenticacion = new MenuAutenticacion(MenuAutenticacion.añadirOpciones());
        while (!salir) {
            try {
                menuAutenticacion.mostrarMenu();
                System.out.print("Ingrese opción : ");
                opcion = Utilidades.ingresoInt();
                switch (opcion) {
                    case 1:
                        Menu menuPersona = usuarios.Persona.iniciarSesion();
                        usuarioActivo = usuarios.Persona.usuarioActivo;
                        if (!(menuPersona == null)) {
                            menuPersona.cargarMenu();                        
                        }
                        break;
                    case 2:
                        usuarios.Persona.registrarUsuario();
                        menuAutenticacion.mostrarMenu();
                        break;
                    case 3:
                        if (menuAutenticacion.salir()) {
                            salir = true;
                        } else {
                            menuAutenticacion.mostrarMenu();
                        }
                        break;
                    default:
                        System.out.println("Opción incorrecta");                       
                }
            } catch (InputMismatchException e) {
                System.out.println("Opción incorrecta");
            }
        }
    }

    public static void inicializarSistema() {
        Tarjeta tarjeta1 = new Tarjeta("Visa", "Crédito");
        Tarjeta tarjeta2 = new Tarjeta("MasterCard", "Débito");
        Tarjeta tarjeta3 = new Tarjeta("Tia", "Afiliacion");
        ArrayList<Tarjeta> tarjetasUser1 = new ArrayList<>();
        tarjetasUser1.add(tarjeta1);
        tarjetasUser1.add(tarjeta2);
        ArrayList<Tarjeta> tarjetasUser2 = new ArrayList<>();
        tarjetasUser2.add(tarjeta2);
        tarjetasUser2.add(tarjeta3);

        Administrador admin = new Administrador("Brank Malatay", "brank@gmail.com", "brank");
        Tarjetahabiente user1 = new Tarjetahabiente("Jose Garcia", "jose@gmail.com", "jose", "28/07/1999", "Guayaquil", tarjetasUser1);
        Tarjetahabiente user2 = new Tarjetahabiente("Juan Mendoza", "juan@gmail.com", "juan", "01/09/1999", "Guayaquil", tarjetasUser2);
        Data.usuarios.add(admin);
        Data.usuarios.add(user1);
        Data.usuarios.add(user2);

        Empresa empresa1 = new Empresa("Tia", "Supermercado", "www.tia.com.ec", "tia", "tia", "tia");
        Establecimiento local1 = new Establecimiento("Guayaquil", "Norte", "Via a Daule", "0999999999".split(","), "10:00 - 21:00", empresa1);
        Promocion promocion1 = new Promocion("Descuento 20%", 30, "10/03/2019", "10/08/2019", local1, tarjeta1);
        local1.getPromociones().add(promocion1);
        empresa1.getEstablecimientos().add(local1);
        Data.empresas.add(empresa1);

        Empresa empresa2 = new Empresa("Burger King", "Comida Rápida", "www.burgerking.com.ec", "burgerkingec", "burgerkingec", "burgerkingec");
        Establecimiento local2 = new Establecimiento("Guayaquil", "Centro", "9 de octubre", "0999999999".split(","), "10:00 - 21:00", empresa2);
        Promocion promocion2 = new Promocion("Descuento 30%", 30, "10/03/2019", "10/08/2019", local2, tarjeta2);
        local2.getPromociones().add(promocion2);
        empresa2.getEstablecimientos().add(local2);
        Data.empresas.add(empresa2);
        
        Empresa empresa3 = new Empresa("Domino's Pizza", "Comida Rápida", "www.dominos.com.ec", "dominospizza", "dominospizza", "dominospizza");
        Establecimiento local3 = new Establecimiento("Guayaquil", "Centenario", "Domingo Comin", "0999999999".split(","), "12:00 - 22:00", empresa3);
        Promocion promocion3 = new Promocion("Descuento 40%", 30, "10/03/2019", "10/08/2019", local3, tarjeta3);
        local3.getPromociones().add(promocion3);
        empresa3.getEstablecimientos().add(local3);
        Data.empresas.add(empresa3);
    }
}
